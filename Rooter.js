import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from './component/DrawerContent';
import LandingScreen from './screens/LandingPage/LandingPage';
import LoginScreen from './screens/LoginPage/LoginPage';
import RegisterScreen from './screens/RegisterPage/RegisterPage';
import HomeScreen from './screens/HomePage/HomePage';
import BookHistoryScreen from './screens/BookHistoryPage/BookHistoryPage';
import ProfileScreen from './screens/ProfilePage/ProfilePage';
import {useSelector, useDispatch} from 'react-redux';
import {handleCheckToken} from './redux/Action/AuthAction';
import ShowFeaturedField from './screens/ShowFeaturedField/ShowFeaturedField';
import FieldDetailPage from './screens/FieldDetailPage/FieldDetailPage';
import FeedbackPage from './screens/FeebbackPage/FeedbackPage';
import SchedulePage from './screens/SchedulePage/SchedulePage';
import BookPage from './screens/BookPage/BookPage';
import TicketPage from './screens/TicketPage/TicketPage';
import EditProfilePage from './screens/EditProfilePage/EditProfilePage';
import CheckoutPage from './screens/CheckoutPage/CheckoutPage';
import PaymentPage from './screens/PaymentPage/PaymentPage';
import ReviewPage from './screens/ReviewPage/ReviewPage';
import EditReviewPage from './screens/EditReviewPage/EditReviewPage';

export default function Rooter() {
  const AuthState = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      dispatch(handleCheckToken());
      setIsLoading(false);
    }, 1000);
  }, [dispatch]);

  const Loading = () => {
    return (
      <View style={styles.loadingScreen}>
        <ActivityIndicator size="large" color="#53C9C2" />
      </View>
    );
  };

  if (isLoading) {
    return <Loading />;
  }

  const AuthStack = createStackNavigator();
  const AuthScreen = ({navigation}) => {
    return (
      <AuthStack.Navigator>
        <AuthStack.Screen
          name="LandingScreen"
          component={LandingScreen}
          options={{headerShown: false}}
        />
        <AuthStack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <AuthStack.Screen
          name="RegisterScreen"
          component={RegisterScreen}
          options={{headerShown: false}}
        />
      </AuthStack.Navigator>
    );
  };

  const HomeStack = createStackNavigator();
  const HomeStackScreen = ({navigation}) => {
    return (
      <HomeStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#313131',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <HomeStack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
            headerRight: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <HomeStack.Screen
          name="FeaturedField"
          component={ShowFeaturedField}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
        <HomeStack.Screen
          name="FieldDetail"
          component={FieldDetailPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
        <HomeStack.Screen
          name="Schedule"
          component={SchedulePage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
        <HomeStack.Screen
          name="Book"
          component={BookPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
        <HomeStack.Screen
          name="TicketHome"
          component={TicketPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.reset({
                    index: 1,
                    routes: [{name: 'History'}],
                  });
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="arrow-left" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </HomeStack.Navigator>
    );
  };

  const ProfileStack = createStackNavigator();
  const ProfileStackScreen = ({navigation}) => {
    return (
      <ProfileStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#313131',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <ProfileStack.Screen
          name="Profile"
          component={ProfileScreen}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
            headerRight: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <ProfileStack.Screen
          name="EditProfile"
          component={EditProfilePage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
      </ProfileStack.Navigator>
    );
  };

  const BookHistoryStack = createStackNavigator();
  const BookHistoryStackScreen = ({navigation}) => {
    return (
      <BookHistoryStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#313131',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <BookHistoryStack.Screen
          name="History"
          component={BookHistoryScreen}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
            headerRight: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <BookHistoryStack.Screen
          name="Feedback"
          component={FeedbackPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
        <BookHistoryStack.Screen
          name="Ticket"
          component={TicketPage}
          options={{
            title: 'TICKET',
            headerTitleStyle: styles.textLogo,
          }}
        />
      </BookHistoryStack.Navigator>
    );
  };

  const CheckoutStack = createStackNavigator();
  const CheckoutStackScreen = ({navigation}) => {
    return (
      <CheckoutStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#313131',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <CheckoutStack.Screen
          name="Checkout"
          component={CheckoutPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
            headerRight: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <CheckoutStack.Screen
          name="Payment"
          component={PaymentPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
      </CheckoutStack.Navigator>
    );
  };

  const ReviewStack = createStackNavigator();
  const ReviewStackScreen = ({navigation}) => {
    return (
      <ReviewStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#313131',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <ReviewStack.Screen
          name="Review"
          component={ReviewPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
            headerRight: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
        <ReviewStack.Screen
          name="EditReview"
          component={EditReviewPage}
          options={{
            title: 'KICKIN',
            headerTitleStyle: styles.textLogo,
          }}
        />
      </ReviewStack.Navigator>
    );
  };

  const Tab = createBottomTabNavigator();
  const TabScreen = () => {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home';
            } else if (route.name === 'History') {
              iconName = focused ? 'book' : 'book';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'user' : 'user';
            }

            return (
              <Icon name={iconName} type="feather" size={size} color={color} />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: '#53C9C2',
          inactiveTintColor: '#919191',
          style: {
            backgroundColor: '#545454',
            height: 60,
            paddingTop: 7,
            borderTopColor: 'transparent',
          },
          labelStyle: {
            marginBottom: 8,
            fontSize: 12,
            fontFamily: 'Montserrat-Medium',
            letterSpacing: 1,
          },
        }}>
        <Tab.Screen name="Home" component={HomeStackScreen} />
        <Tab.Screen name="History" component={BookHistoryStackScreen} />
        <Tab.Screen name="Profile" component={ProfileStackScreen} />
      </Tab.Navigator>
    );
  };

  const Drawer = createDrawerNavigator();
  const DrawerScreen = () => {
    return (
      <Drawer.Navigator
        drawerContentOptions={{
          activeTintColor: '#53C9C2',
        }}
        drawerContent={(props) => <DrawerContent {...props} />}>
        <Drawer.Screen
          name="HomeScreen"
          component={TabScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>Home</Text>,
            drawerIcon: () => (
              <Icon name="home" type="feather" size={23} color="#fff" />
            ),
          }}
        />
        <Drawer.Screen
          name="CheckoutScreen"
          component={CheckoutStackScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>Checkout</Text>,
            drawerIcon: () => (
              <Icon
                name="shopping-cart"
                type="font-awesome"
                size={23}
                color="#fff"
              />
            ),
          }}
        />
        <Drawer.Screen
          name="ReviewStackScreen"
          component={ReviewStackScreen}
          options={{
            drawerLabel: () => <Text style={styles.text}>My Review</Text>,
            drawerIcon: () => (
              <Icon
                name="rate-review"
                type="materialicons"
                size={23}
                color="#fff"
              />
            ),
          }}
        />
      </Drawer.Navigator>
    );
  };

  const RootStack = createStackNavigator();
  return (
    <NavigationContainer>
      <RootStack.Navigator>
        {!AuthState.token ? (
          <RootStack.Screen
            name="Auth"
            component={AuthScreen}
            options={{headerShown: false}}
          />
        ) : (
          <RootStack.Screen
            name="App"
            component={DrawerScreen}
            options={{headerShown: false}}
          />
        )}
      </RootStack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  loadingScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#313131',
  },
  text: {
    fontFamily: 'Montserrat-Medium',
    color: '#fff',
    letterSpacing: 2,
  },
  textLogo: {
    fontFamily: 'Montserrat-BoldItalic',
    fontSize: 24,
    color: '#53C9C2',
  },
});

import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
var PushNotification = require('react-native-push-notification');
import moment from 'moment';
import {Icon} from 'react-native-elements';

export default function BookHistoryItem({item, navigation}) {
  useEffect(() => {
    if (item.status !== 'expired') {
      let time = item.schedule_time;
      let subTime = time.substring(0, 2);
      const datePlay = moment(`${item.schedule_date} ${subTime}`);
      const dateNotification = moment(datePlay - 60 * 1000 * 60);

      PushNotification.localNotificationSchedule({
        message: `Be ready to kick you're game start in an hour at ${item.Field.Store.store_name} ${item.Field.name}`,
        date: new Date(dateNotification),
        vibrate: true,
        largeIcon: 'ic_launcher_round',
        smallIcon: 'ic_launcher_round',
      });
    }
  }, [item]);

  PushNotification.configure({
    onRegister: function (token) {},

    onNotification: function (notification) {
      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    onAction: function (notification) {},

    onRegistrationError: function (err) {},

    permissions: {
      alert: true,
      badge: true,
      sound: true,
    },

    popInitialNotification: true,

    requestPermissions: true,
  });
  return (
    <View style={styles.cardView}>
      <View style={styles.cardViewHeader}>
        <View style={styles.content}>
          <Text style={styles.label}>{item.Field.name}</Text>
          <Text style={styles.contentDesc}>{item.Field.Store.store_name}</Text>
        </View>
        {item.status === 'expired' ? (
          <View style={{alignSelf: 'center'}}>
            <Icon
              name="alert-circle"
              type="feather"
              size={20}
              color="#ED4C67"
            />
            <Text style={styles.expired}>Expired</Text>
          </View>
        ) : (
          <View style={styles.content}>
            <Text style={styles.label}>{item.schedule_date}</Text>
            <Text style={styles.contentDesc}>{item.schedule_time}</Text>
          </View>
        )}
      </View>
      <View style={styles.cardViewContent}>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('Ticket', {
              item: item,
            })
          }>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>View Ticket</Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('Feedback', {
              id: item.Field.id,
            })
          }>
          <View>
            <Text style={styles.btnText}>Give Feedback</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cardView: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 15,
    backgroundColor: '#545454',
    borderRadius: 5,
  },
  cardViewHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 25,
  },
  cardViewContent: {
    marginVertical: 10,
  },
  fieldName: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    letterSpacing: 1,
    color: '#F4F4F4',
    alignSelf: 'center',
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    height: 50,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
  btnText: {
    alignSelf: 'center',
    marginTop: 10,
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    color: '#53C9C2',
  },
  content: {
    marginVertical: 2,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 2,
    color: '#fff',
    marginVertical: 2,
  },
  contentDesc: {
    fontFamily: 'Montserrat-Medium',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 10,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 2,
  },
  expired: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 10,
    letterSpacing: 2,
    color: '#ED4C67',
    marginVertical: 2,
  },
});

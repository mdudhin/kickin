import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Pagination} from 'react-native-snap-carousel';

export default function CarouselItem({item, length, activeSlide, navigation}) {
  return (
    <View>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('FieldDetail', {
            id: item.id,
          })
        }>
        <Image
          style={styles.image}
          source={{uri: item.Image_fields[0].image_field_url}}
        />
      </TouchableOpacity>
      <Pagination
        dotsLength={length}
        activeDotIndex={activeSlide}
        containerStyle={styles.pagination}
        dotStyle={styles.dotPagination}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  image: {
    width: width - 70,
    height: height / 4,
    borderRadius: 10,
    alignSelf: 'center',
  },
  text: {
    color: '#fff',
    position: 'absolute',
    bottom: 10,
    left: 5,
  },
  pagination: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: -15,
  },
  dotPagination: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 8,
    backgroundColor: '#fff',
  },
});

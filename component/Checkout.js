import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Item from './CheckoutItem';
import {useDispatch} from 'react-redux';
import {getCheckout} from '../redux/Action/BookingAction';

export default function Checkout({navigation, checkout}) {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <FlatList
        data={checkout}
        keyExtractor={(item) => `${item.id}`}
        refreshing={false}
        onRefresh={() => dispatch(getCheckout())}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
});

import React from 'react';
import {StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';

export default function CheckoutItem({item, navigation}) {
  return (
    <View style={styles.cardView}>
      <View style={styles.cardViewHeader}>
        <Text style={styles.fieldName}>{item.Field.name}</Text>
        <View style={styles.dateTime}>
          <Text style={styles.date}>{item.schedule_date}</Text>
          <Text style={styles.time}>{item.schedule_time}</Text>
        </View>
      </View>
      <View style={styles.cardViewContent}>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('Payment', {
              item: item,
            })
          }>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>View Payment</Text>
          </View>
        </TouchableWithoutFeedback>
        <Text style={styles.btnText}>{item.status}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cardView: {
    marginVertical: 10,
    marginHorizontal: 10,
    paddingVertical: 15,
    backgroundColor: '#545454',
    borderRadius: 5,
  },
  cardViewHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  cardViewContent: {
    marginVertical: 10,
  },
  fieldName: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    letterSpacing: 1,
    color: '#F4F4F4',
    alignSelf: 'center',
    width: 170,
  },
  dateTime: {
    marginHorizontal: 8,
  },
  date: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 2,
  },
  time: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    letterSpacing: 1,
    color: 'rgba(255, 255, 255, 0.7)',
    marginVertical: 2,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    height: 50,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
  btnText: {
    alignSelf: 'center',
    marginTop: 10,
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    color: '#53C9C2',
    textTransform: 'uppercase',
  },
});

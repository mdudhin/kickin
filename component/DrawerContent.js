import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import {ListItem, Icon} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {handleLogout} from '../redux/Action/AuthAction';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {getUser} from '../redux/Action/ProfileAction';
import Modal from 'react-native-modal';

const DrawerContent = (props) => {
  const dispatch = useDispatch();
  const [isModalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  const ProfileState = useSelector((state) => state.profile);
  const profile = ProfileState.user;

  const confirmModal = () => {
    setModalVisible(!isModalVisible);
  };

  const logout = () => {
    setModalVisible(!isModalVisible);
    dispatch(handleLogout());
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image
          source={{
            uri:
              profile.avatar == null
                ? 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png'
                : profile.avatar,
          }}
          style={styles.image}
        />
        <Text style={styles.name}>{profile.name}</Text>
        <Text style={styles.position}>
          {profile.favorite_position == null
            ? 'Position'
            : profile.favorite_position}
        </Text>
      </View>
      <DrawerContentScrollView {...props} style={styles.content}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <ListItem
        title={<Text style={styles.title}>Log Out</Text>}
        containerStyle={{backgroundColor: '#545454'}}
        leftIcon={
          <Icon
            name="exit-to-app"
            type="materialicons"
            size={25}
            color="#fff"
          />
        }
        onPress={() => confirmModal()}
      />
      <Modal isVisible={isModalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.logoutModal}>
            <Text style={styles.logoutText}>Are you sure to log out ?</Text>
            <View style={styles.actionModal}>
              <TouchableWithoutFeedback onPress={logout}>
                <View style={styles.modalBtn}>
                  <Text style={styles.modalBtnText}>Yes</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={confirmModal}>
                <View style={styles.modalBtn}>
                  <Text style={styles.modalBtnText}>No</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#545454',
  },
  header: {
    backgroundColor: '#313131',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 250,
  },
  content: {
    paddingVertical: 10,
  },
  title: {
    fontFamily: 'Montserrat-Medium',
    textAlign: 'center',
    paddingLeft: 20,
    letterSpacing: 2,
    color: '#fff',
  },
  image: {
    height: 110,
    width: 110,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
    marginVertical: 13,
  },
  name: {
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    textAlign: 'center',
    letterSpacing: 2,
    marginBottom: 5,
    marginHorizontal: 20,
  },
  position: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-MediumItalic',
    color: '#fff',
    borderColor: '#53C9C2',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 8,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
  logoutModal: {
    backgroundColor: '#313131',
    margin: 50,
    borderRadius: 5,
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  logoutText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    letterSpacing: 1,
  },
  actionModal: {
    flexDirection: 'row',
    marginTop: 20,
    alignSelf: 'center',
  },
  modalBtn: {
    backgroundColor: '#545454',
    width: 90,
    height: 32,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  modalBtnText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
    color: '#53C9C2',
  },
});

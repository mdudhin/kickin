import React, {useEffect} from 'react';
import {StyleSheet, View, FlatList, Text} from 'react-native';
import Item from './FeaturedFieldItem';
import {useDispatch, useSelector} from 'react-redux';
import {getField} from '../redux/Action/FieldAction';
import {Icon} from 'react-native-elements';

export default function FeaturedField({navigation}) {
  const dispatch = useDispatch();
  const fieldState = useSelector((state) => state.field);

  useEffect(() => {
    dispatch(getField());
  }, [dispatch]);

  const allField = fieldState.allField;

  return (
    <View style={styles.container}>
      {allField.length === 0 ? (
        <View style={styles.noData}>
          <Icon
            name="box-open"
            type="font-awesome-5"
            size={70}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.noDataText}>There is no any data yet !</Text>
        </View>
      ) : (
        <FlatList
          data={allField}
          keyExtractor={(item) => `${item.id}`}
          horizontal
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => {
            return <Item item={item} navigation={navigation} />;
          }}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  carousel: {
    flex: 1,
  },
  noData: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 60,
  },
  noDataText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 10,
  },
});

import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
  Image,
  Dimensions,
} from 'react-native';
import Rupiah from './Rupiah';

export default function FeaturedFieldItem({item, navigation}) {
  return (
    <View style={styles.cardView}>
      <Image
        style={styles.image}
        source={{uri: item.Image_fields[0].image_field_url}}
      />
      <View style={styles.cardViewContent}>
        <View style={styles.label}>
          <Text style={styles.fieldName}>{item.name}</Text>
          <Text style={styles.fieldStoreName}>{item.Store.store_name}</Text>
          <Text style={styles.price}>Rp. {Rupiah(item.price)}</Text>
        </View>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('FieldDetail', {
              id: item.id,
            })
          }>
          <View style={styles.btnSecondary}>
            <Text style={styles.textBtnSecondary}>View</Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('Schedule', {
              id: item.id,
              field: item,
            })
          }>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>Book</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    width: width / 2.3,
    height: 350,
    marginVertical: 10,
    marginHorizontal: 10,
    backgroundColor: '#545454',
    borderRadius: 10,
  },
  image: {
    width: width / 2.3,
    height: 250 / 1.8,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  cardViewContent: {
    alignItems: 'center',
    paddingVertical: 13,
    paddingHorizontal: 10,
  },
  label: {
    height: 95,
  },
  fieldName: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    textAlign: 'center',
    letterSpacing: 1,
    color: '#F4F4F4',
    marginBottom: 5,
    paddingHorizontal: 10,
  },
  fieldStoreName: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    textAlign: 'center',
    letterSpacing: 1,
    color: '#F4F4F4',
    marginBottom: 8,
    paddingHorizontal: 10,
  },
  price: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Medium',
    color: '#fff',
    borderColor: '#53C9C2',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 8,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    minWidth: 100,
    maxWidth: 137,
    height: 34,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
  btnSecondary: {
    minWidth: 100,
    maxWidth: 137,
    height: 34,
    marginHorizontal: 25,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#53C9C2',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
  },
  textBtnSecondary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#53C9C2',
  },
});

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Icon} from 'react-native-elements';

export default function LoginForm({
  email,
  wrongEmail,
  trueEmail,
  wrongEmailStatement,
  password,
  wrongPassword,
  wrongPasswordStatement,
  secureTextEntry,
  handleEmail,
  handlePassword,
  updateSecureTextEntry,
  login,
  navigation,
  handleEmailValidation,
  signIn,
}) {
  const IconCheck = () => {
    if (trueEmail) {
      return (
        <Icon name="check-circle" type="feather" size={20} color="#53C9C2" />
      );
    } else {
      return <View />;
    }
  };

  return (
    <View>
      <View style={styles.action}>
        <Icon
          name="user-o"
          type="font-awesome"
          size={20}
          color="rgba(255, 255, 255, 0.8)"
        />
        <TextInput
          placeholder="Enter Email"
          placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
          autoCapitalize="none"
          style={styles.textInput}
          keyboardType="email-address"
          onChangeText={(val) => handleEmail(val)}
          onBlur={(val) => handleEmailValidation(val)}
        />
        {wrongEmail ? (
          <Icon name="alert-circle" type="feather" size={20} color="#ED4C67" />
        ) : (
          <IconCheck />
        )}
      </View>
      {wrongEmail ? (
        <Text style={styles.wrongStatement}>{wrongEmailStatement}</Text>
      ) : (
        <Text />
      )}
      <View style={styles.action}>
        <Icon
          name="lock"
          type="feather"
          size={20}
          color="rgba(255, 255, 255, 0.8)"
        />
        <TextInput
          placeholder="Enter Password"
          placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
          autoCapitalize="none"
          secureTextEntry={secureTextEntry ? true : false}
          onChangeText={(val) => handlePassword(val)}
          style={styles.textInput}
        />
        <TouchableOpacity onPress={updateSecureTextEntry}>
          {secureTextEntry ? (
            <Icon
              name="eye-off"
              type="feather"
              size={20}
              color="rgba(255, 255, 255, 0.8)"
            />
          ) : (
            <Icon
              name="eye"
              type="feather"
              size={20}
              color="rgba(255, 255, 255, 0.8)"
            />
          )}
        </TouchableOpacity>
      </View>
      {wrongPassword ? (
        <Text style={styles.wrongStatement}>{wrongPasswordStatement}</Text>
      ) : (
        <Text />
      )}
      <View style={styles.actionButton}>
        <TouchableOpacity>
          <Text style={styles.forgotPassword}>Forgot Password ?</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={login} style={styles.btnPrimary}>
          <Text style={styles.textBtnPrimary}>Log In</Text>
        </TouchableOpacity>
        <Text style={styles.actionDivider}>OR</Text>
        <TouchableOpacity style={styles.btnFacebook}>
          <Icon
            name="google"
            type="font-awesome"
            size={24}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.textBtnFacebook} onPress={() => signIn()}>
            Log In With Google
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
          <Text style={styles.signUp}>Don’t have an account? Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  action: {
    flexDirection: 'row',
    backgroundColor: '#545454',
    marginTop: 15,
    borderRadius: 5,
    height: 60,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    marginHorizontal: 25,
  },
  textInput: {
    flex: 1,
    marginTop: -15,
    paddingLeft: 20,
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
  },
  wrongStatement: {
    paddingLeft: 25,
    paddingTop: 7,
    color: '#ED4C67',
  },
  actionButton: {
    flexDirection: 'column',
    paddingVertical: 15,
  },
  forgotPassword: {
    color: '#fff',
    paddingLeft: 25,
    paddingTop: 5,
    paddingBottom: 30,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    height: 60,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
  btnFacebook: {
    flexDirection: 'row',
    backgroundColor: '#EC4536',
    height: 60,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  textBtnFacebook: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: 'rgba(255, 255, 255, 0.8)',
    marginHorizontal: 20,
  },
  actionDivider: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 15,
    color: 'rgba(255, 255, 255, 0.8)',
    alignSelf: 'center',
    marginVertical: 15,
  },
  signUp: {
    alignSelf: 'center',
    paddingTop: 15,
    fontSize: 15,
    color: '#53C9C2',
  },
});

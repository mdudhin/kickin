import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Item from './MyReviewItem';
import {useDispatch} from 'react-redux';
import {getMyReview} from '../redux/Action/ReviewAction';

export default function MyReview({review, handleLoadMore, navigation}) {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <FlatList
        data={review}
        keyExtractor={(item) => `${item.id}`}
        refreshing={false}
        onEndReached={handleLoadMore}
        onRefresh={() => dispatch(getMyReview())}
        onEndReachedThreshold={0.5}
        initialNumToRender={5}
        horizontal={false}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 25,
  },
});

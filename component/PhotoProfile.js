import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

const PhotoProfile = ({photoProfile, handleChoosePhoto}) => {
  const ProfileState = useSelector((state) => state.profile);
  const profile = ProfileState.user;

  return (
    <View>
      <Image
        source={
          photoProfile == null
            ? {
                uri:
                  profile.avatar == null
                    ? 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png'
                    : profile.avatar,
              }
            : {
                uri: photoProfile,
              }
        }
        style={styles.image}
      />
      <TouchableOpacity style={styles.btn} onPress={handleChoosePhoto}>
        <Text style={styles.imageText}>Pick Image</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PhotoProfile;

const styles = StyleSheet.create({
  btn: {
    paddingTop: 15,
  },
  imageText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#53C9C2',
    textAlign: 'center',
  },
  image: {
    height: 130,
    width: 130,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
  },
});

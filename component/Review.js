import React from 'react';
import {View} from 'react-native';
import Item from './ReviewItem';

export default function Review({review}) {
  const ItemList = () => {
    return review.map((item, i) => <Item key={i} item={item} />);
  };
  return (
    <View>
      <ItemList />
    </View>
  );
}

const Rupiah = (price) => {
  let priceString = `${price}`,
    rest = priceString.length % 3,
    rp = priceString.substr(0, rest),
    thousand = priceString.substr(rest).match(/\d{3}/g);

  if (thousand) {
    let separator = rest ? '.' : '';
    rp += separator + thousand.join('.');
  }
  return rp;
};

export default Rupiah;

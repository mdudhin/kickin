import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

export default function TimeScheduleItem({item, navigation, field}) {
  return !item.availability ? (
    <View style={styles.cardViewDark}>
      <Text style={styles.text}>{item.schedule_time}</Text>
    </View>
  ) : (
    <TouchableOpacity
      style={styles.cardView}
      onPress={() =>
        navigation.navigate('Book', {
          item: item,
          field: field,
        })
      }>
      <Text style={styles.text}>{item.schedule_time}</Text>
    </TouchableOpacity>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 10,
    paddingVertical: 10,
    backgroundColor: '#53C9C2',
    borderRadius: 5,
  },
  cardViewDark: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 10,
    paddingVertical: 10,
    backgroundColor: '#CECECE',
    borderRadius: 5,
  },
  text: {
    color: '#fff',
    fontSize: 15,
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 2,
    alignSelf: 'center',
  },
});

import Axios from 'axios';
import {
  REQUEST_LOGIN,
  CHECK_TOKEN,
  REGISTER,
  FAILED_LOGIN,
  FAILED_REGISTER,
  SHOW_LOADING,
} from '../Type/auth';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';

export const handleCheckToken = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      dispatch({type: CHECK_TOKEN, token: token});
    } catch (error) {
      ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const handleLogin = (email, password) => {
  return async (dispatch) => {
    dispatch({type: SHOW_LOADING, isLoading: true});
    try {
      const res = await Axios.post(
        'https://be-kickin.herokuapp.com/api/v1/user/login',
        {
          email: email,
          password: password,
        },
      );
      const token = res.data.data.token;
      await AsyncStorage.setItem('@token', token);
      const message = 'Success to login';
      dispatch({
        type: REQUEST_LOGIN,
        token: token,
        email: email,
      });
      ToastAndroid.show(message, ToastAndroid.SHORT);
    } catch (e) {
      dispatch({type: FAILED_LOGIN});
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const handleRegister = (name, email, password) => {
  return async (dispatch) => {
    dispatch({type: SHOW_LOADING, isLoading: true});
    try {
      const res = await Axios.post(
        'https://be-kickin.herokuapp.com/api/v1/user/register',
        {
          email: email,
          password: password,
          name: name,
        },
      );
      const token = res.data.data.token;
      await AsyncStorage.setItem('@token', token);
      const message = 'Success to Register';
      dispatch({type: REGISTER, token: token, email: email});
      ToastAndroid.show(message, ToastAndroid.SHORT);
    } catch (e) {
      dispatch({type: FAILED_REGISTER});
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const handleLogout = () => {
  return async (dispatch) => {
    try {
      await AsyncStorage.clear();
      dispatch({type: REQUEST_LOGIN, token: null, email: null});
    } catch (error) {
      ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

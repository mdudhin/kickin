import Axios from 'axios';
import {GET_FIELD, GET_DETAIL_FIELD, GET_CAROUSEL} from '../Type/field';
import {ToastAndroid} from 'react-native';

export const getCarousel = () => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        'https://be-kickin.herokuapp.com/api/v1/field/ratingDesc?page=1',
      );
      if (res !== null) {
        const data = res.data.data.result;
        dispatch({
          type: GET_CAROUSEL,
          data: data.slice(0, 4),
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getField = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.fields;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldPriceDesc = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/priceDesc?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.fields;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldPriceAsc = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/priceAsc?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.fields;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldNameDesc = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/nameDesc?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.fields;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldNameAsc = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/nameAsc?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.fields;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldRatingDesc = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/ratingDesc?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.result;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldRatingAsc = (page, field) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/ratingAsc?page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.result;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getFieldFilter = (page, field, filter) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/type?page=${
          page === undefined ? 1 : page
        }&field_type=${filter}`,
      );
      if (res !== null) {
        const data = res.data.data.filterField;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const searchFieldByStore = (page, field, store) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/search?store_name=${store}&page=${
          page === undefined ? 1 : page
        }`,
      );
      if (res !== null) {
        const data = res.data.data.fields;
        dispatch({
          type: GET_FIELD,
          data: page === undefined ? data : [...field, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

export const getDetailField = (id) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/field/one/${id}`,
      );
      if (res !== null) {
        const data = res.data.data.field;
        dispatch({
          type: GET_DETAIL_FIELD,
          data: data,
          store: data.Store,
          review: data.Reviews,
        });
      }
    } catch (e) {
      ToastAndroid.show('Fail to get data', ToastAndroid.SHORT);
    }
  };
};

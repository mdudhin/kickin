import {GET_USER, EDIT_USER, SHOW_LOADING_PROFILE} from '../Type/profile';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {ToastAndroid} from 'react-native';

export const getUser = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await Axios.get(
        'https://be-kickin.herokuapp.com/api/v1/profile/show',
        {
          headers: {
            Authorization: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data.profile;
        dispatch({
          type: GET_USER,
          user: data,
        });
      }
    } catch (e) {
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const editUser = (
  nameEdit,
  mottoEdit,
  photoName,
  photoProfileEdit,
  ageEdit,
  favTeamEdit,
  favPositionEdit,
) => {
  return async (dispatch) => {
    dispatch({
      type: SHOW_LOADING_PROFILE,
      isLoading: true,
    });
    try {
      const token = await AsyncStorage.getItem('@token');
      const data = new FormData();
      if (nameEdit !== null) {
        data.append('name', nameEdit);
      }
      if (mottoEdit !== null) {
        data.append('motto', mottoEdit);
      }
      if (photoProfileEdit !== null) {
        data.append('image', {
          uri: photoProfileEdit,
          name: photoName,
          type: 'image/jpg',
        });
      }
      if (ageEdit !== null) {
        data.append('age', ageEdit);
      }
      if (favTeamEdit !== null) {
        data.append('favorite_team', favTeamEdit);
      }
      if (favPositionEdit !== null) {
        data.append('favorite_position', favPositionEdit);
      }
      Axios.put('https://be-kickin.herokuapp.com/api/v1/profile/', data, {
        headers: {
          Authorization: token,
          'Content-Type': 'application/json',
        },
      })
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to edit user !';
            dispatch({
              type: EDIT_USER,
              message: message,
            });
            dispatch(getUser());
            ToastAndroid.show(message, ToastAndroid.SHORT);
          }
        })
        .catch((error) => {
          dispatch({
            type: SHOW_LOADING_PROFILE,
            isLoading: false,
          });
          ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
        });
    } catch (e) {
      dispatch({
        type: SHOW_LOADING_PROFILE,
        isLoading: false,
      });
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

import Axios from 'axios';
import {GET_SCHEDULE, GET_SCHEDULE_TIME} from '../Type/schedule';
import {ToastAndroid} from 'react-native';

export const getSchedule = (id) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/schedule/showall/${id}`,
      );
      if (res !== null) {
        let dates = [];
        res.data.data.schedulesUpdated.map((item) => {
          return dates.push(item.date);
        });
        const uniqueDates = [...new Set(dates)];
        dispatch({
          type: GET_SCHEDULE,
          data: uniqueDates,
        });
      }
    } catch (e) {
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const getScheduleTime = (id, date) => {
  return async (dispatch) => {
    try {
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/schedule/${id}?date=${date}`,
      );
      if (res !== null) {
        const data = res.data.data.schedulesUpdated;
        dispatch({
          type: GET_SCHEDULE_TIME,
          data: data,
        });
      }
    } catch (e) {
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

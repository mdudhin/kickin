import {
  REQUEST_LOGIN,
  CHECK_TOKEN,
  REGISTER,
  FAILED_LOGIN,
  FAILED_REGISTER,
  SHOW_LOADING,
  HIDE_RESPONSE,
  SHOW_RESPONSE,
} from '../Type/auth';

const initialState = {
  isLoading: false,
  isResponse: false,
  token: null,
  email: null,
  message: null,
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECK_TOKEN:
      return {
        ...state,
        token: action.token,
      };
    case SHOW_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case REQUEST_LOGIN:
      return {
        ...state,
        token: action.token,
        email: action.email,
        isLoading: false,
      };
    case REGISTER:
      return {
        ...state,
        token: action.token,
        email: action.email,
        isLoading: false,
      };
    case FAILED_LOGIN:
      return {
        ...state,
        isLoading: false,
      };
    case FAILED_REGISTER:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

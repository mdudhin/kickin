import {
  GET_CHECKOUT,
  FAILED_GET_CHECKOUT,
  GET_BOOKING,
  FAILED_GET_BOOKING,
  CREATE_BOOKING,
  PAY_BOOKING,
  SHOW_LOADING,
} from '../Type/booking';

const initialState = {
  checkout: [],
  booking: [],
  message: null,
  isLoading: false,
};

export const BookingReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CHECKOUT:
      return {
        ...state,
        checkout: action.data,
        isLoading: false,
      };
    case SHOW_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case FAILED_GET_CHECKOUT:
      return {
        ...state,
        message: action.message,
      };
    case GET_BOOKING:
      return {
        ...state,
        booking: action.data,
      };
    case FAILED_GET_BOOKING:
      return {
        ...state,
        message: action.message,
      };
    case CREATE_BOOKING:
      return {
        ...state,
        message: action.message,
      };
    case PAY_BOOKING:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

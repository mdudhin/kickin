import {
  GET_REVIEW,
  FAILED_GET_REVIEW,
  ADD_REVIEW,
  FAILED_ADD_REVIEW,
  GET_MY_REVIEW,
  DELETE_REVIEW,
  EDIT_REVIEW,
} from '../Type/review';

const initialState = {
  review: 0,
  myReview: [],
  message: null,
};

export const ReviewReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_REVIEW:
      return {
        ...state,
        review: action.data,
      };
    case GET_MY_REVIEW:
      return {
        ...state,
        myReview: action.data,
      };
    case FAILED_GET_REVIEW:
      return {
        ...state,
        message: action.message,
      };
    case ADD_REVIEW:
      return {
        ...state,
        message: action.message,
      };
    case EDIT_REVIEW:
      return {
        ...state,
        message: action.message,
      };
    case DELETE_REVIEW:
      return {
        ...state,
        message: action.message,
      };
    default:
      return state;
  }
};

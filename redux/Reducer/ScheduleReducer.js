import {
  GET_SCHEDULE,
  FAILED_GET_SCHEDULE,
  GET_SCHEDULE_TIME,
  FAILED_GET_SCHEDULE_TIME,
} from '../Type/schedule';

const initialState = {
  schedule: [],
  time: [],
  message: null,
};

export const ScheduleReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SCHEDULE:
      return {
        ...state,
        schedule: action.data,
      };
    case FAILED_GET_SCHEDULE:
      return {
        ...state,
        message: action.message,
      };
    case GET_SCHEDULE_TIME:
      return {
        ...state,
        time: action.data,
      };
    case FAILED_GET_SCHEDULE_TIME:
      return {
        ...state,
        message: action.message,
      };
    default:
      return state;
  }
};

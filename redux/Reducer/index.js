import {combineReducers} from 'redux';
import {AuthReducer} from './AuthReducer';
import {ProfileReducer} from './ProfileReducer';
import {FieldReducer} from './FieldReducer';
import {ReviewReducer} from './ReviewReducer';
import {ScheduleReducer} from './ScheduleReducer';
import {BookingReducer} from './BookingReducer';

export default combineReducers({
  auth: AuthReducer,
  profile: ProfileReducer,
  field: FieldReducer,
  review: ReviewReducer,
  schedule: ScheduleReducer,
  booking: BookingReducer,
});

import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import BookHistory from '../../component/BookHistory';
import {useDispatch, useSelector} from 'react-redux';
import {getBooking} from '../../redux/Action/BookingAction';
import {Icon} from 'react-native-elements';
import {useFocusEffect} from '@react-navigation/native';

export default function BookHistoryPage({navigation}) {
  const dispatch = useDispatch();
  const bookingState = useSelector((state) => state.booking);

  const [page, setPage] = useState(2);

  useFocusEffect(
    React.useCallback(() => {
      dispatch(getBooking());
    }, [dispatch]),
  );

  const booking = bookingState.booking;

  const handleLoadMore = () => {
    const pagePlus = page + 1;
    setPage(pagePlus);
    dispatch(getBooking(page, booking));
  };

  return (
    <View style={styles.container}>
      {booking.length === 0 ? (
        <View style={styles.noData}>
          <Icon
            name="box-open"
            type="font-awesome-5"
            size={70}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.noDataText}>There is no any data yet !</Text>
        </View>
      ) : (
        <BookHistory
          navigation={navigation}
          handleLoadMore={handleLoadMore}
          booking={booking}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  noData: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 10,
  },
});

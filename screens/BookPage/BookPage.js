import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Rupiah from '../../component/Rupiah';
import FormatDate from '../../component/FormatDate';
import {useDispatch} from 'react-redux';
import {createBooking} from '../../redux/Action/BookingAction';
import {CommonActions} from '@react-navigation/native';

export default function BookPage({route, navigation}) {
  const {item, field} = route.params;

  const dispatch = useDispatch();

  const book = () => {
    dispatch(createBooking(field.id, item.date, item.schedule_time));
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          {
            name: 'CheckoutScreen',
          },
        ],
      }),
    );
  };

  return (
    <ScrollView style={styles.container}>
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../assets/image/kickin.jpeg')}
          style={styles.image}
        />
        <LinearGradient
          locations={[0, 0.3]}
          colors={['rgba(49, 49, 49, 0)', '#313131']}
          style={styles.gradient}
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.fieldName}>{field.name}</Text>
        <Text style={styles.price}>Rp. {Rupiah(field.price)},00</Text>
      </View>
      <View style={styles.field}>
        <Text style={styles.label}>Store</Text>
        <Text style={styles.fieldContent}>{field.Store.store_name}</Text>
      </View>
      <View style={styles.field}>
        <Text style={styles.label}>Store</Text>
        <Text style={styles.fieldContent}>{field.Store.address}</Text>
      </View>
      <View style={styles.field}>
        <Text style={styles.label}>Date</Text>
        <Text style={styles.fieldContent}>{FormatDate(item.date)}</Text>
      </View>
      <View style={styles.field}>
        <Text style={styles.label}>Time</Text>
        <Text style={styles.fieldContent}>{item.schedule_time}</Text>
      </View>
      <View style={styles.action}>
        <TouchableOpacity onPress={() => book()}>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>Book</Text>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
  },
  image: {
    width: null,
    height: 320,
  },
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '95%',
  },
  content: {
    marginTop: 100,
    marginBottom: 40,
  },
  fieldName: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
    letterSpacing: 2,
    color: '#53C9C2',
    marginTop: 15,
    alignSelf: 'center',
  },
  price: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
    color: '#fff',
    borderColor: '#53C9C2',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 8,
    marginTop: 10,
  },
  field: {
    marginHorizontal: 25,
    marginVertical: 8,
  },
  fieldContent: {
    fontFamily: 'Montserrat-Medium',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 2,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 2,
    color: '#fff',
  },
  action: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 25,
    marginBottom: 20,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    width: 120,
    height: 60,
    marginHorizontal: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
});

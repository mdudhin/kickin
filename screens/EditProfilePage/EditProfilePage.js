import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import PhotoProfile from '../../component/PhotoProfile';
import {useDispatch, useSelector} from 'react-redux';
import {getUser, editUser} from '../../redux/Action/ProfileAction';
import ImagePicker from 'react-native-image-picker';
import Menu, {MenuItem} from 'react-native-material-menu';
import Modal from 'react-native-modal';

export default function EditProfilePage({navigation}) {
  const dispatch = useDispatch();

  const [photoProfile, setPhotoProfile] = useState(null);
  const [photoName, setPhotoName] = useState(null);
  const [photoPath, setPhotoPath] = useState(null);

  const [name, setName] = useState(null);
  const [wrongName, setWrongName] = useState(false);

  const [favPosition, setFavPosition] = useState(null);
  const [favTeam, setFavTeam] = useState(null);
  const [motto, setMotto] = useState(null);
  const [age, setAge] = useState(null);

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  const ProfileState = useSelector((state) => state.profile);
  const profile = ProfileState.user;

  const handleChoosePhoto = () => {
    const options = {
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        ToastAndroid.show('Cancelled', ToastAndroid.SHORT);
      } else if (response.error) {
        ToastAndroid.show(response.error, ToastAndroid.SHORT);
      } else {
        setPhotoProfile(response.uri);
        setPhotoName(response.fileName);
        setPhotoPath(response);
      }
    });
  };

  const handleName = (val) => {
    setName(val);
  };

  const handleFavTeam = (val) => {
    setFavTeam(val);
  };

  const handleMotto = (val) => {
    setMotto(val);
  };

  const handleAge = (val) => {
    setAge(val);
  };

  const menu = useRef();

  const showMenu = () => menu.current.show();

  const chooseGk = () => {
    setFavPosition('GK');

    menu.current.hide();
  };

  const chooseFixo = () => {
    setFavPosition('Fixo');

    menu.current.hide();
  };

  const chooseAla = () => {
    setFavPosition('Ala');

    menu.current.hide();
  };

  const choosePivot = () => {
    setFavPosition('Pivot');

    menu.current.hide();
  };

  const updateProfile = () => {
    const nameEdit = name == null ? profile.name : name;
    const mottoEdit = motto == null ? profile.motto : motto;
    const photoProfileEdit =
      photoProfile == null ? profile.avatar : photoProfile;
    const ageEdit = age == null ? profile.age : age;
    const favPositionEdit =
      favPosition == null ? profile.favorite_position : favPosition;
    const favTeamEdit = favTeam == null ? profile.favorite_team : favTeam;
    dispatch(
      editUser(
        nameEdit,
        mottoEdit,
        photoName,
        photoProfileEdit,
        ageEdit,
        favTeamEdit,
        favPositionEdit,
      ),
    );
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <PhotoProfile
          photoProfile={photoProfile}
          handleChoosePhoto={handleChoosePhoto}
        />
      </View>
      <View style={styles.form}>
        <Text style={styles.label}>Name</Text>
        <View style={styles.action}>
          <TextInput
            placeholder="Enter Name"
            placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
            defaultValue={name == null ? profile.name : name}
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={(val) => handleName(val)}
          />
        </View>
      </View>
      {wrongName ? (
        <Text style={styles.wrongStatement}>The form must be filled</Text>
      ) : (
        <View />
      )}
      <View style={styles.form}>
        <Text style={styles.label}>Age</Text>
        <View style={styles.action}>
          <TextInput
            placeholder="Enter Age"
            placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
            defaultValue={
              age == null
                ? profile.age == null
                  ? '0'
                  : profile.age.toString()
                : age
            }
            autoCapitalize="none"
            keyboardType="number-pad"
            style={styles.textInput}
            onChangeText={(val) => handleAge(val)}
          />
        </View>
      </View>
      <View style={styles.form}>
        <Text style={styles.label}>Favorite Position</Text>
        <TouchableWithoutFeedback onPress={showMenu}>
          <View style={styles.action}>
            <Text style={styles.textPosition}>
              {favPosition == null
                ? profile.favorite_position == null
                  ? 'Select Your Position'
                  : profile.favorite_position
                : favPosition}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <Menu ref={menu} style={styles.menu}>
          <ScrollView>
            <MenuItem style={styles.menuItem} onPress={chooseGk}>
              <Text style={styles.selectText}>GK (Goalkeeper)</Text>
            </MenuItem>
            <MenuItem style={styles.menuItem} onPress={chooseFixo}>
              <Text style={styles.selectText}>Fixo (Fixed Defender)</Text>
            </MenuItem>
            <MenuItem style={styles.menuItem} onPress={chooseAla}>
              <Text style={styles.selectText}>Ala (Left/Right Wings)</Text>
            </MenuItem>
            <MenuItem style={styles.menuItem} onPress={choosePivot}>
              <Text style={styles.selectText}>Pivot (Attacker)</Text>
            </MenuItem>
          </ScrollView>
        </Menu>
      </View>
      <View style={styles.form}>
        <Text style={styles.label}>Favorite Team</Text>
        <View style={styles.action}>
          <TextInput
            placeholder="Enter You're Favorite Team"
            placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
            defaultValue={favTeam == null ? profile.favorite_team : favTeam}
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={(val) => handleFavTeam(val)}
          />
        </View>
      </View>
      <View style={styles.form}>
        <Text style={styles.label}>Motto</Text>
        <View style={styles.actionMotto}>
          <TextInput
            placeholder="Enter Motto"
            placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
            autoCapitalize="none"
            defaultValue={motto == null ? profile.motto : motto}
            multiline={true}
            numberOfLines={4}
            style={styles.textInput}
            onChangeText={(val) => handleMotto(val)}
          />
        </View>
      </View>
      <TouchableWithoutFeedback onPress={() => updateProfile()}>
        <View style={styles.btnPrimary}>
          <Text style={styles.textBtnPrimary}>Save Profile</Text>
        </View>
      </TouchableWithoutFeedback>
      <Modal isVisible={ProfileState.isLoading}>
        <View style={styles.centeredView}>
          <ActivityIndicator size="large" color="#53C9C2" />
        </View>
      </Modal>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 50,
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  form: {
    marginVertical: 10,
    marginHorizontal: 25,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 2,
    color: '#fff',
  },
  action: {
    flexDirection: 'row',
    backgroundColor: '#545454',
    marginTop: 15,
    borderRadius: 5,
    height: 60,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  actionMotto: {
    flexDirection: 'row',
    backgroundColor: '#545454',
    marginTop: 15,
    borderRadius: 5,
    height: 80,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  textInput: {
    flex: 1,
    marginTop: -17,
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
  },
  wrongStatement: {
    paddingLeft: 25,
    color: '#d63031',
  },
  btnPrimary: {
    backgroundColor: '#545454',
    height: 60,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 30,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#53C9C2',
  },
  textPosition: {
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
  },
  menu: {
    backgroundColor: '#545454',
    width: 200,
    height: 100,
  },
  menuItem: {
    backgroundColor: '#545454',
    width: 200,
  },
  selectText: {
    color: '#fff',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
  loadingScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '50%',
  },
});

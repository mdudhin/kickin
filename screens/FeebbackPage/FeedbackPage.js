import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {addReview} from '../../redux/Action/ReviewAction';
import {useDispatch} from 'react-redux';

export default function FeedbackPage({route, navigation}) {
  const {id} = route.params;

  const dispatch = useDispatch();

  const [defaultRating, setDefaultRating] = useState();
  const [maxRating, setMaxRating] = useState(5);
  const [inputReview, setInputReview] = useState();

  let rating = [];
  for (var i = 1; i <= maxRating; i++) {
    rating.push(
      <TouchableOpacity activeOpacity={0.7} key={i}>
        <Image
          style={styles.StarImage}
          source={
            i <= defaultRating
              ? {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                }
              : {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                }
          }
        />
      </TouchableOpacity>,
    );
  }
  const handleReview = (val) => {
    setInputReview(val);
  };
  const addReviewData = () => {
    dispatch(addReview(id, inputReview, defaultRating));
    setInputReview();
    setDefaultRating();
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.feedback}>Rate this field</Text>
      <Text style={styles.subFeedback}>Tell others what you think</Text>
      <View style={styles.addReview}>
        <View style={styles.addRating}>
          {rating.map((data, i) => (
            <TouchableOpacity onPress={() => setDefaultRating(i)} key={i}>
              <Image
                style={styles.addStarImage}
                source={
                  i <= defaultRating
                    ? {
                        uri:
                          'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                      }
                    : {
                        uri:
                          'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                      }
                }
              />
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.formReview}>
          <View style={styles.actionForm}>
            <TextInput
              placeholder="Describe your experience"
              placeholderTextColor="rgba(255, 255, 255, 0.7)"
              autoCapitalize="none"
              multiline={true}
              numberOfLines={4}
              style={styles.textInput}
              value={inputReview}
              onChangeText={(val) => handleReview(val)}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => addReviewData()}
          style={styles.btnAddReview}>
          <View style={styles.iconAddReview}>
            <Icon name="send" type="font-awesome" size={20} color="#313131" />
          </View>
          <Text style={styles.textBtnReview}>Send</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
  },
  feedback: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 18,
    letterSpacing: 2,
    color: '#fff',
    marginVertical: 15,
    marginHorizontal: 35,
  },
  subFeedback: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 10,
    letterSpacing: 1,
    color: 'rgba(255, 255, 255, 0.7)',
    marginTop: -10,
    marginHorizontal: 35,
  },
  addReview: {
    marginVertical: 20,
    marginBottom: 35,
  },
  addRating: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 15,
  },
  addStarImage: {
    width: 40,
    height: 40,
    marginHorizontal: 5,
    resizeMode: 'cover',
  },
  actionForm: {
    flexDirection: 'row',
    backgroundColor: '#525252',
    marginTop: 15,
    marginHorizontal: 40,
    borderRadius: 5,
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    flex: 1,
    paddingLeft: 12,
    paddingTop: 20,
    paddingBottom: 25,
    color: '#fff',
  },
  btnAddReview: {
    backgroundColor: '#53C9C2',
    height: 50,
    paddingHorizontal: 30,
    borderRadius: 5,
    marginVertical: 20,
    marginHorizontal: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  textBtnReview: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
    marginHorizontal: 5,
  },
  iconAddReview: {
    marginHorizontal: 5,
  },
});

import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'react-native-elements';
import Review from '../../component/Review';
import {useDispatch, useSelector} from 'react-redux';
import {getDetailField} from '../../redux/Action/FieldAction';
import Rupiah from '../../component/Rupiah';
import {getReview, addReview} from '../../redux/Action/ReviewAction';

export default function FieldDetailPage({route, navigation}) {
  const {id} = route.params;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDetailField(id));
    dispatch(getReview(id));
  }, [dispatch, id]);

  const detailFieldState = useSelector((state) => state.field);
  const field = detailFieldState.detailField;
  const store = detailFieldState.store;
  const review = detailFieldState.review;

  const overallState = useSelector((state) => state.review.review);

  const [defaultRating, setDefaultRating] = useState();
  const [maxRating, setMaxRating] = useState(5);
  const [inputReview, setInputReview] = useState();

  let rating = [];
  //Array to hold the filled or empty Stars
  for (var i = 1; i <= maxRating; i++) {
    rating.push(
      <TouchableOpacity activeOpacity={0.7} key={i}>
        <Image
          style={styles.StarImage}
          source={
            i <= defaultRating
              ? {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                }
              : {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                }
          }
        />
      </TouchableOpacity>,
    );
  }
  const handleReview = (val) => {
    setInputReview(val);
  };

  const addReviewData = () => {
    dispatch(addReview(id, inputReview, defaultRating));
    setInputReview();
    setDefaultRating();
  };

  const image =
    Array.isArray(field.Image_fields) &&
    field.Image_fields.length &&
    field.Image_fields[0].image_field_url;

  return (
    <ScrollView style={styles.container}>
      <View>
        <Image source={{uri: `${image}`}} style={styles.image} />
        <LinearGradient
          locations={[0, 0.95]}
          colors={['rgba(49, 49, 49, 0)', '#313131']}
          style={styles.gradient}
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.fieldName}>{field.name}</Text>
        <Text style={styles.price}>Rp. {Rupiah(field.price)},00</Text>
        <View style={styles.field}>
          <Text style={styles.label}>Store</Text>
          <Text style={styles.fieldContent}>{store.store_name}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Phone Number</Text>
          <Text style={styles.fieldContent}>{store.phone_number}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Address</Text>
          <Text style={styles.fieldContent}>{store.address}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Field Type</Text>
          <Text style={styles.fieldContent}>{field.field_type}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Facilities</Text>
          <Text style={styles.fieldContent}>{store.facilities}</Text>
        </View>
      </View>
      <View style={styles.action}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('Schedule', {
              field: field,
            })
          }>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>Book Now</Text>
          </View>
        </TouchableOpacity>
      </View>
      <Text style={styles.feedback}>Rate this field</Text>
      <Text style={styles.subFeedback}>Tell others what you think</Text>
      <View style={styles.addReview}>
        <View style={styles.addRating}>
          {rating.map((data, i) => (
            <TouchableOpacity onPress={() => setDefaultRating(i)} key={i}>
              <Image
                style={styles.addStarImage}
                source={
                  i <= defaultRating
                    ? {
                        uri:
                          'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                      }
                    : {
                        uri:
                          'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                      }
                }
              />
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.formReview}>
          <View style={styles.actionForm}>
            <TextInput
              placeholder="Describe your experience"
              placeholderTextColor="rgba(255, 255, 255, 0.7)"
              autoCapitalize="none"
              multiline={true}
              numberOfLines={4}
              style={styles.textInput}
              value={inputReview}
              onChangeText={(val) => handleReview(val)}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => addReviewData()}
          style={styles.btnAddReview}>
          <View style={styles.iconAddReview}>
            <Icon name="send" type="font-awesome" size={20} color="#313131" />
          </View>
          <Text style={styles.textBtnReview}>Send</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.feedback}>Ratings and Reviews</Text>
      <View style={styles.rating}>
        <Image
          style={styles.starImage}
          source={{
            uri:
              'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
          }}
        />
        <Text style={styles.ratingTxt}>{overallState}/5</Text>
      </View>
      <View style={styles.review}>
        <Review review={review} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
  },
  image: {
    width: null,
    height: 320,
  },
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '100%',
  },
  content: {
    marginTop: -60,
  },
  fieldName: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
    letterSpacing: 2,
    textAlign: 'center',
    color: '#53C9C2',
    marginTop: 5,
    alignSelf: 'center',
    marginHorizontal: 50,
  },
  field: {
    marginHorizontal: 25,
    marginVertical: 8,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 2,
    color: '#fff',
    marginVertical: 2,
  },
  fieldContent: {
    fontFamily: 'Montserrat-Medium',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 2,
  },
  price: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
    color: '#fff',
    borderColor: '#53C9C2',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 8,
    marginTop: 15,
    marginBottom: 20,
  },
  feedback: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 18,
    letterSpacing: 2,
    color: '#fff',
    marginVertical: 15,
    marginHorizontal: 25,
  },
  subFeedback: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 10,
    letterSpacing: 1,
    color: 'rgba(255, 255, 255, 0.7)',
    marginTop: -10,
    marginHorizontal: 25,
  },
  rating: {
    flexDirection: 'row',
    marginHorizontal: 25,
  },
  starImage: {
    width: 26,
    height: 26,
    marginHorizontal: 1,
    resizeMode: 'cover',
  },
  ratingTxt: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 2,
    color: '#fff',
    marginHorizontal: 5,
    marginVertical: 3,
  },
  review: {
    marginVertical: 20,
  },
  action: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    width: 120,
    height: 60,
    marginHorizontal: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  btnSecondary: {
    backgroundColor: '#545454',
    width: 120,
    height: 60,
    marginHorizontal: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
  textBtnSecondary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#53C9C2',
  },
  addReview: {
    marginVertical: 20,
    marginBottom: 35,
  },
  addRating: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 15,
  },
  addStarImage: {
    width: 40,
    height: 40,
    marginHorizontal: 5,
    resizeMode: 'cover',
  },
  actionForm: {
    flexDirection: 'row',
    backgroundColor: '#525252',
    marginTop: 15,
    marginHorizontal: 40,
    borderRadius: 5,
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    flex: 1,
    paddingLeft: 12,
    paddingTop: 20,
    paddingBottom: 25,
    color: '#fff',
  },
  btnAddReview: {
    backgroundColor: '#53C9C2',
    height: 50,
    paddingHorizontal: 30,
    borderRadius: 5,
    marginVertical: 20,
    marginHorizontal: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  textBtnReview: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
    marginHorizontal: 5,
  },
  iconAddReview: {
    marginHorizontal: 5,
  },
});

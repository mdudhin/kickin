import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  StatusBar,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default function LandingPage({navigation}) {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../assets/image/kickin.jpeg')}
          style={styles.image}
        />
        <LinearGradient
          locations={[0, 0.6]}
          colors={['rgba(49, 49, 49, 0)', '#313131']}
          style={styles.gradient}
        />
      </View>
      <View style={styles.logo}>
        <Text style={styles.textLogo}>KICKIN</Text>
        <Text style={styles.textMotto}>Let's Play</Text>
      </View>
      <View style={styles.action}>
        <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>Log In</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
          <View style={styles.btnSecondary}>
            <Text style={styles.textBtnSecondary}>Sign Up</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: '#313131',
  },
  image: {
    width: null,
    height: 492,
  },
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '95%',
  },
  logo: {
    height: height / 2.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLogo: {
    fontFamily: 'Montserrat-BoldItalic',
    fontSize: 35,
    color: '#53C9C2',
  },
  textMotto: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 18,
    color: '#F4F4F4',
    marginTop: 20,
  },
  action: {
    height: height / 4.2,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    height: 60,
    marginHorizontal: 40,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  btnSecondary: {
    backgroundColor: '#545454',
    height: 60,
    marginHorizontal: 40,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
  textBtnSecondary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#53C9C2',
  },
});

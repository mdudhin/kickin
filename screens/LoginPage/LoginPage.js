import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import LoginForm from '../../component/LoginForm';
import {useDispatch, useSelector} from 'react-redux';
import {handleLogin, handleResponse} from '../../redux/Action/AuthAction';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import Modal from 'react-native-modal';

export default function LoginPage({navigation}) {
  const dispatch = useDispatch();
  const AuthState = useSelector((state) => state.auth);

  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [userInfo, setUserInfo] = useState();

  const handleEmail = (val) => {
    setEmail(val);
  };

  const handleEmailValidation = () => {
    if (email != '') {
      setWrongEmail(false);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
    } else {
      setWrongEmail(true);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);
    if (password != '') {
      setWrongPassword(false);
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const login = async () => {
    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email != '' && password != '' && regex.test(email)) {
      dispatch(handleLogin(email, password));
    } else {
      ToastAndroid.show('Failed to Log In', ToastAndroid.SHORT);
    }
  };

  useEffect(() => {
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '<FROM DEVELOPER CONSOLE>', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      //hostedDomain: '', // specifies a hosted domain restriction
      //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      //accountName: '', // [Android] specifies an account name on the device that should be used
      iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }, []);

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userData = await GoogleSignin.signIn();
      setUserInfo(userData);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={styles.logo}>
        <Text style={styles.textLogo}>KICKIN</Text>
      </View>
      <LoginForm
        email={email}
        wrongEmail={wrongEmail}
        trueEmail={trueEmail}
        wrongEmailStatement={wrongEmailStatement}
        password={password}
        wrongPassword={wrongPassword}
        wrongPasswordStatement={wrongPasswordStatement}
        secureTextEntry={secureTextEntry}
        handleEmail={handleEmail}
        handlePassword={handlePassword}
        updateSecureTextEntry={updateSecureTextEntry}
        login={login}
        navigation={navigation}
        handleEmailValidation={handleEmailValidation}
        signIn={signIn}
      />
      <Modal isVisible={AuthState.isLoading}>
        <View style={styles.centeredView}>
          <ActivityIndicator size="large" color="#53C9C2" />
        </View>
      </Modal>
    </ScrollView>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#313131',
  },
  logo: {
    marginTop: 30,
    marginBottom: 40,
    alignItems: 'center',
  },
  textLogo: {
    fontFamily: 'Montserrat-BoldItalic',
    fontSize: 30,
    color: '#53C9C2',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
});

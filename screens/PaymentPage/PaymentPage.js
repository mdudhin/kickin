import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {Icon} from 'react-native-elements';
import Rupiah from '../../component/Rupiah';
import {useDispatch} from 'react-redux';
import {payBooking} from '../../redux/Action/BookingAction';

export default function PaymentPage({route, navigation}) {
  const {item} = route.params;
  const [imagePayment, setImagePayment] = useState(null);
  const [imageName, setImageName] = useState(null);
  const [imagePath, setImagePath] = useState(null);

  const dispatch = useDispatch();

  const handleChoosePhoto = () => {
    const options = {
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        ToastAndroid.show('Cancelled', ToastAndroid.SHORT);
      } else if (response.error) {
        ToastAndroid.show(response.error, ToastAndroid.SHORT);
      } else {
        setImagePayment(response.uri);
        setImageName(response.fileName);
        setImagePath(response);
      }
    });
  };

  const payment = () => {
    dispatch(payBooking(item.id, imagePayment, imageName));
    navigation.navigate('Checkout');
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.field}>
        <Text style={styles.label}>Field Name</Text>
        <Text style={styles.fieldContent}>{item.Field.name}</Text>
      </View>
      <View style={styles.field}>
        <Text style={styles.label}>Date</Text>
        <Text style={styles.fieldContent}>{item.schedule_date}</Text>
      </View>
      <View style={styles.field}>
        <Text style={styles.label}>Time</Text>
        <Text style={styles.fieldContent}>{item.schedule_time}</Text>
      </View>
      <View style={styles.payment}>
        <Text style={styles.labelPayment}>Payment Info</Text>
        <View style={styles.field}>
          <Text style={styles.label}>Bank</Text>
          <Text style={styles.fieldContent}>{item.Field.Store.bank_name}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Account Name</Text>
          <Text style={styles.fieldContent}>
            {item.Field.Store.account_name}
          </Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Account Number</Text>
          <Text style={styles.fieldContent}>
            {item.Field.Store.account_number}
          </Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Fee</Text>
          <Text style={styles.fieldContent}>
            Rp. {Rupiah(item.Field.price)},00
          </Text>
        </View>
        <View style={styles.paymentSlip}>
          <Text style={styles.label}>Upload Payment Slip</Text>
          <View style={styles.inputImage}>
            {item.status !== 'validating' ? (
              <TouchableOpacity style={styles.btn} onPress={handleChoosePhoto}>
                {imagePayment == null ? (
                  <Icon name="plus" type="feather" size={60} color="#53C9C2" />
                ) : (
                  <Icon name="check" type="feather" size={60} color="#53C9C2" />
                )}
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.btn}>
                <Icon name="check" type="feather" size={60} color="#53C9C2" />
              </TouchableOpacity>
            )}
            <Image
              source={
                imagePayment == null
                  ? {
                      uri: imagePayment,
                    }
                  : {
                      uri: imagePayment,
                    }
              }
              style={styles.image}
            />
          </View>
        </View>
      </View>
      {item.status !== 'validating' ? (
        <TouchableWithoutFeedback onPress={() => payment()}>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>Send Payment Slip</Text>
          </View>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback>
          <View style={styles.btnPrimary}>
            <Text style={styles.textBtnPrimary}>Wait For Validating</Text>
          </View>
        </TouchableWithoutFeedback>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  payment: {
    marginHorizontal: 25,
    marginVertical: 8,
  },
  labelPayment: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    letterSpacing: 2,
    color: '#53C9C2',
    marginVertical: 12,
  },
  field: {
    marginHorizontal: 25,
    marginVertical: 8,
  },
  fieldContent: {
    fontFamily: 'Montserrat-Medium',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 2,
  },
  paymentSlip: {
    marginHorizontal: 25,
    marginVertical: 15,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 2,
    color: '#fff',
  },
  inputImage: {
    flexDirection: 'row',
    marginVertical: 20,
  },
  btn: {
    width: 120,
    height: 120,
    backgroundColor: '#545454',
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 120,
    width: 120,
    borderRadius: 5,
    resizeMode: 'cover',
    marginHorizontal: 20,
  },
  btnPrimary: {
    backgroundColor: '#53C9C2',
    height: 60,
    marginHorizontal: 25,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 30,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#313131',
  },
});

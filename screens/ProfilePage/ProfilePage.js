import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getUser} from '../../redux/Action/ProfileAction';

export default function ProfilePage({navigation}) {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  const ProfileState = useSelector((state) => state.profile);
  const profile = ProfileState.user;

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <Image
          source={{
            uri:
              profile.avatar == null
                ? 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png'
                : profile.avatar,
          }}
          style={styles.avatar}
        />
        <Text style={styles.name}>{profile.name}</Text>
      </View>
      <View style={styles.content}>
        <Text style={styles.position}>
          {profile.favorite_position == null
            ? 'Position'
            : profile.favorite_position}
        </Text>
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Age</Text>
        <Text style={styles.contentDesc}>{profile.age}</Text>
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Play</Text>
        <Text style={styles.contentDesc}>{profile.booking_number}</Text>
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Favorite Team</Text>
        <Text style={styles.contentDesc}>{profile.favorite_team}</Text>
      </View>
      <View style={styles.content}>
        <Text style={styles.label}>Motto</Text>
        <Text style={styles.contentDesc}>{profile.motto}</Text>
      </View>
      <TouchableWithoutFeedback
        onPress={() => navigation.navigate('EditProfile')}>
        <View style={styles.btnPrimary}>
          <Text style={styles.textBtnPrimary}>Edit Profile</Text>
        </View>
      </TouchableWithoutFeedback>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 55,
    paddingHorizontal: 20,
  },
  avatar: {
    height: 130,
    width: 130,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
    borderWidth: 1,
    borderColor: '#53C9C2',
  },
  name: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 20,
    textAlign: 'center',
    letterSpacing: 2,
    color: '#fff',
    marginTop: 20,
    marginHorizontal: 30,
  },
  position: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-MediumItalic',
    color: '#fff',
    borderColor: '#53C9C2',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 8,
  },
  content: {
    marginHorizontal: 25,
    marginVertical: 8,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 2,
    color: '#fff',
    marginVertical: 2,
  },
  contentDesc: {
    fontFamily: 'Montserrat-Medium',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 2,
  },
  btnPrimary: {
    backgroundColor: '#545454',
    height: 60,
    marginHorizontal: 40,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 30,
  },
  textBtnPrimary: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    color: '#53C9C2',
  },
});

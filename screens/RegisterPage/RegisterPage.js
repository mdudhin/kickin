import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';
import RegisterForm from '../../component/RegisterForm';
import {useSelector, useDispatch} from 'react-redux';
import {handleRegister} from '../../redux/Action/AuthAction';
import {ScrollView} from 'react-native-gesture-handler';
import Modal from 'react-native-modal';

export default function RegisterPage({navigation}) {
  const dispatch = useDispatch();
  const AuthState = useSelector((state) => state.auth);

  const [name, setName] = useState('');
  const [wrongName, setWrongName] = useState(false);

  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const [password1, setPassword1] = useState('');
  const [wrongPassword1, setWrongPassword1] = useState(false);
  const [wrongPasswordStatement1, setWrongPasswordStatement1] = useState(false);
  const [secureTextEntry1, setSecureTextEntry1] = useState(true);

  const handleName = (val) => {
    setName(val);
  };

  const handleEmail = (val) => {
    setEmail(val);
  };

  const handleEmailValidation = () => {
    if (email != '') {
      setWrongEmail(false);
      setTrueEmail(true);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    } else {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);
  };

  const handlePasswordValidation = () => {
    if (password != '') {
      setWrongPassword(false);
    }

    const passwordLength = /^(?=.{6})/;
    const passwordNumber = /^(?=.*[0-9])/;

    if (!passwordLength.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 6 character');
    } else if (!passwordNumber.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 1 numeric');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }
  };

  const handleConfirmPassword = (val) => {
    setPassword1(val);

    if (password != '') {
      setWrongPassword1(false);
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const updateSecureTextEntry1 = () => {
    setSecureTextEntry1(!secureTextEntry1);
  };

  const register = async () => {
    if (name == '') {
      setWrongName(true);
    } else {
      setWrongName(false);
      setTrueEmail(true);
    }

    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password != password1) {
      setWrongPassword1(true);
      setWrongPasswordStatement1('Password does not match');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordLength = /^(?=.{6})/;
    const passwordNumber = /^(?=.*[0-9])/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    } else {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Wrong email format');
    }

    if (!passwordLength.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 6 character');
    } else if (!passwordNumber.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 1 numeric');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    if (
      name != '' &&
      email != '' &&
      password != '' &&
      password == password1 &&
      regex.test(email) &&
      passwordLength.test(password) &&
      passwordNumber.test(password)
    ) {
      dispatch(handleRegister(name, email, password));
    } else {
      ToastAndroid.show('Failed to register', ToastAndroid.SHORT);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={styles.logo}>
        <Text style={styles.textLogo}>KICKIN</Text>
      </View>
      <RegisterForm
        name={name}
        wrongName={wrongName}
        email={email}
        wrongEmail={wrongEmail}
        trueEmail={trueEmail}
        wrongEmailStatement={wrongEmailStatement}
        password={password}
        wrongPassword={wrongPassword}
        wrongPasswordStatement={wrongPasswordStatement}
        secureTextEntry={secureTextEntry}
        password1={password1}
        wrongPassword1={wrongPassword1}
        wrongPasswordStatement1={wrongPasswordStatement1}
        secureTextEntry1={secureTextEntry1}
        handleName={handleName}
        handleEmail={handleEmail}
        handlePassword={handlePassword}
        updateSecureTextEntry={updateSecureTextEntry}
        handleConfirmPassword={handleConfirmPassword}
        updateSecureTextEntry1={updateSecureTextEntry1}
        register={register}
        navigation={navigation}
        handleEmailValidation={handleEmailValidation}
        handlePasswordValidation={handlePasswordValidation}
      />
      <Modal isVisible={AuthState.isLoading}>
        <View style={styles.centeredView}>
          <ActivityIndicator size="large" color="#53C9C2" />
        </View>
      </Modal>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#313131',
  },
  logo: {
    marginTop: 20,
    marginBottom: 30,
    alignItems: 'center',
  },
  textLogo: {
    fontFamily: 'Montserrat-BoldItalic',
    fontSize: 30,
    color: '#53C9C2',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
});

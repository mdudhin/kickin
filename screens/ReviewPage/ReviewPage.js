import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MyReview from '../../component/MyReview';
import {useDispatch, useSelector} from 'react-redux';
import {getMyReview} from '../../redux/Action/ReviewAction';
import {Icon} from 'react-native-elements';

export default function ReviewPage({navigation}) {
  const dispatch = useDispatch();
  const bookingState = useSelector((state) => state.review);

  const [page, setPage] = useState(2);

  useEffect(() => {
    dispatch(getMyReview());
  }, [dispatch]);

  const review = bookingState.myReview;

  const handleLoadMore = () => {
    const pagePlus = page + 1;
    setPage(pagePlus);
    dispatch(getMyReview(page, review));
  };
  return (
    <View style={styles.container}>
      {review.length === 0 ? (
        <View style={styles.noData}>
          <Icon
            name="box-open"
            type="font-awesome-5"
            size={70}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.noDataText}>There is no any data yet !</Text>
        </View>
      ) : (
        <MyReview
          review={review}
          handleLoadMore={handleLoadMore}
          navigation={navigation}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  noData: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 10,
  },
});

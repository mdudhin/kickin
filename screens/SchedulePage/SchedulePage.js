import React, {useRef, useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Rupiah from '../../component/Rupiah';
import Menu, {MenuItem} from 'react-native-material-menu';
import {useDispatch, useSelector} from 'react-redux';
import {getSchedule, getScheduleTime} from '../../redux/Action/ScheduleAction';
import TimeSchedule from '../../component/TimeSchedule';
import FormatDate from '../../component/FormatDate';

export default function SchedulePage({route, navigation}) {
  const {field} = route.params;

  const [selectedDate, setSelectedDate] = useState();

  const dispatch = useDispatch();
  const scheduleState = useSelector((state) => state.schedule);

  const schedule = scheduleState.schedule;

  useEffect(() => {
    dispatch(getSchedule(field.id));
  }, [dispatch, field]);

  const menu = useRef();

  const showMenu = () => menu.current.show();

  const getTime = (date) => {
    setSelectedDate(date);
    menu.current.hide();
    dispatch(getScheduleTime(field.id, date));
  };

  const time = scheduleState.time;

  return (
    <View style={styles.container}>
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../assets/image/kickin.jpeg')}
          style={styles.image}
        />
        <LinearGradient
          locations={[0, 0.25]}
          colors={['rgba(49, 49, 49, 0)', '#313131']}
          style={styles.gradient}
        />
      </View>
      <View style={styles.content}>
        <Text style={styles.fieldName}>{field.name}</Text>
        <Text style={styles.price}>Rp. {Rupiah(field.price)},00</Text>
      </View>
      <View style={styles.action}>
        <Menu
          ref={menu}
          button={
            <View style={styles.sort}>
              <Text style={styles.sortText} onPress={showMenu}>
                {schedule.length === -0
                  ? 'Not Available'
                  : selectedDate != null
                  ? FormatDate(selectedDate)
                  : 'Select Date'}
              </Text>
            </View>
          }
          style={styles.menu}>
          <ScrollView>
            {schedule.map((item, i) => (
              <MenuItem key={i} item={item} onPress={() => getTime(item)}>
                <Text style={styles.selectText}>{FormatDate(item)}</Text>
              </MenuItem>
            ))}
          </ScrollView>
        </Menu>
      </View>
      <View style={styles.guide}>
        <View style={styles.item}>
          <View style={styles.boxPrimary} />
          <Text style={styles.boxText}>Available</Text>
        </View>
        <View style={styles.item}>
          <View style={styles.boxSecondary} />
          <Text style={styles.boxText}>Not Available</Text>
        </View>
      </View>
      {selectedDate != null ? (
        <TimeSchedule time={time} navigation={navigation} field={field} />
      ) : (
        <View style={styles.message}>
          <Text style={styles.messageText}>Not select any date yet</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
  },
  image: {
    width: null,
    height: 320,
  },
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: '100%',
  },
  content: {
    marginTop: 100,
    marginBottom: 20,
  },
  fieldName: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
    letterSpacing: 2,
    color: '#53C9C2',
    marginTop: 15,
    alignSelf: 'center',
  },
  price: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 1,
    color: '#fff',
    borderColor: '#53C9C2',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 2,
    paddingHorizontal: 8,
    marginTop: 10,
  },
  action: {
    flexDirection: 'column',
    alignSelf: 'center',
    marginBottom: 20,
  },
  menu: {
    width: 300,
    maxHeight: 200,
    alignItems: 'center',
    marginHorizontal: 25,
    marginTop: 10,
    backgroundColor: '#545454',
  },
  selectText: {
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
  },
  sort: {
    backgroundColor: '#545454',
    width: 300,
    paddingVertical: 10,
    marginHorizontal: 25,
    marginTop: 10,
    borderRadius: 5,
  },
  sortText: {
    alignSelf: 'center',
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
  },
  message: {
    marginVertical: 50,
    alignSelf: 'center',
  },
  messageText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    letterSpacing: 2,
    color: 'rgba(255, 255, 255, 0.8)',
  },
  guide: {
    flexDirection: 'row',
    marginBottom: 15,
    alignSelf: 'center',
  },
  item: {
    flexDirection: 'row',
    marginHorizontal: 10,
  },
  boxPrimary: {
    backgroundColor: '#53C9C2',
    width: 25,
    height: 25,
    borderRadius: 5,
  },
  boxSecondary: {
    backgroundColor: '#CECECE',
    width: 25,
    height: 25,
    borderRadius: 5,
  },
  boxText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 11,
    color: 'rgba(255, 255, 255, 0.8)',
    alignSelf: 'center',
    marginHorizontal: 7,
  },
});

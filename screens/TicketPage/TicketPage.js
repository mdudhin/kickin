import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import FormatDate from '../../component/FormatDate';
import Rupiah from '../../component/Rupiah';
import {Icon} from 'react-native-elements';

export default function TicketPage({route, navigation}) {
  const {item} = route.params;
  return (
    <ScrollView style={styles.container}>
      <View style={styles.ticketHeader}>
        <Image
          source={require('../../assets/image/kickin.jpeg')}
          style={styles.image}
        />
        <View style={styles.ticketHeaderText}>
          <Text style={styles.fieldName}>{item.Field.name}</Text>
          <View style={styles.field}>
            <Text style={styles.label}>Play Time</Text>
            <Text style={styles.fieldContent}>{item.schedule_time}</Text>
          </View>
          <View style={styles.field}>
            <Text style={styles.label}>Rental Fee</Text>
            <Text style={styles.fieldContent}>
              Rp. {Rupiah(item.Field.price)},00
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.ticketCenter}>
        <View style={styles.field}>
          <Text style={styles.label}>Store Name</Text>
          <Text style={styles.fieldContent}>{item.Field.Store.store_name}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Store Address</Text>
          <Text style={styles.fieldContent}>{item.Field.Store.address}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Booking Code</Text>
          <Text style={styles.fieldContent}>{item.booking_code}</Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Date</Text>
          <Text style={styles.fieldContent}>
            {FormatDate(item.schedule_date)}
          </Text>
        </View>
        <View style={styles.field}>
          <Text style={styles.label}>Ticket Status</Text>
          <Text style={styles.fieldContent}>{item.status}</Text>
        </View>
      </View>
      <View style={styles.divider} />
      <View style={styles.ticketGuide}>
        <View style={styles.information}>
          <View style={styles.labelWithIcon}>
            <Icon name="question" type="evilicon" size={28} color="#53C9C2" />
            <Text style={styles.labelText}>How to use you're ticket</Text>
          </View>
          <Text style={styles.informationContent}>
            1. Visit the futsal field that you choose
          </Text>
          <Text style={styles.informationContent}>
            2. Show this ticket to the counter at least 5 minute before the game
          </Text>
          <Text style={styles.informationContent}>
            3. If do you want to share this ticket to your friends, you only
            need to screenshot this ticket
          </Text>
          <Text style={styles.informationContent}>
            4. Our counter will check your booking code so make sure keep the
            booking code protected.
          </Text>
          <Text style={styles.informationContent}>5. You're ready to Kick</Text>
        </View>
      </View>
    </ScrollView>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
  },
  ticketHeader: {
    backgroundColor: 'rgba(84, 84, 84, 0.5)',
    flexDirection: 'row',
    minHeight: 200,
    borderTopStartRadius: 5,
    borderTopEndRadius: 5,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginHorizontal: 25,
    marginTop: 10,
  },
  ticketCenter: {
    backgroundColor: '#545454',
    minHeight: 140,
    paddingHorizontal: 25,
    paddingVertical: 20,
    justifyContent: 'center',
    marginHorizontal: 25,
  },
  divider: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#fff',
    marginHorizontal: 25,
  },
  ticketGuide: {
    backgroundColor: '#545454',
    minHeight: 140,
    paddingHorizontal: 25,
    marginHorizontal: 25,
    marginBottom: 25,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  information: {
    marginVertical: 15,
  },
  image: {
    width: 120,
    height: 160,
    borderRadius: 5,
  },
  ticketHeaderText: {
    flex: 1,
    marginHorizontal: 30,
  },
  textLogo: {
    fontFamily: 'Montserrat-BoldItalic',
    fontSize: 24,
    color: '#53C9C2',
    marginVertical: 5,
  },
  fieldName: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    letterSpacing: 2,
    textAlign: 'justify',
    color: '#53C9C2',
    marginVertical: 5,
  },
  field: {
    marginVertical: 8,
  },
  label: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 2,
    color: '#fff',
  },
  fieldContent: {
    fontFamily: 'Montserrat-Medium',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 2,
  },
  labelWithIcon: {
    flexDirection: 'row',
    marginLeft: -5,
    marginBottom: 7,
  },
  labelText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 2,
    color: '#fff',
    alignSelf: 'center',
    marginLeft: 5,
  },
  informationContent: {
    fontFamily: 'Montserrat-Medium',
    color: '#fff',
    fontSize: 10,
    letterSpacing: 1,
    textAlign: 'justify',
    marginVertical: 3,
  },
});
